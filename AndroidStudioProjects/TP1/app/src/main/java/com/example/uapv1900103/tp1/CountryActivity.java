package com.example.uapv1900103.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewDebug;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.Toast;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);


        TextView tv = (TextView)findViewById(R.id.tv);
        final EditText ed1 = (EditText)findViewById(R.id.ed1);
        final EditText ed2 = (EditText)findViewById(R.id.ed2);
        final EditText ed3 = (EditText)findViewById(R.id.ed3);
        final EditText ed4 = (EditText)findViewById(R.id.ed4);
        final EditText ed5 = (EditText)findViewById(R.id.ed5);
        final ImageView iv = (ImageView)findViewById(R.id.iv);
        final Button btn = (Button)findViewById(R.id.btn);
        //btn.setOnClickListener((OnClickListener)this);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("cp");
        String ct = bundle.getString("pays");

        tv.setText(ct);

        final CountryList cnl = new CountryList();
        final country n = cnl.getCountry(ct);

        String cpt = n.getmCapital();
        ed1.setText(cpt);

        String language = n.getmLanguage();
        ed2.setText(language);

        String dev = n.getmCurrency();
        ed3.setText(dev);

        int pop = n.getmPopulation();
        ed4.setText(String.valueOf(pop));

        int sup = n.getmArea();
        ed5.setText(String.valueOf(sup));

        String drapeau = n.getmImgFile();
        int identifier = getApplicationContext().getResources().getIdentifier(drapeau, "drawable",getApplicationContext().getPackageName());

        iv.setImageResource(identifier);

        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                n.setmCapital(String.valueOf(ed1.getText()));
                n.setmLanguage(String.valueOf(ed2.getText()));
                n.setmCurrency(String.valueOf(ed3.getText()));
                int pop  = Integer.valueOf(String.valueOf(ed4.getText()));
                n.setmPopulation(pop);
                n.setmArea(Integer.parseInt(ed5.getText().toString()));

                cnl.sauvgarde(n);
                Toast.makeText(CountryActivity.this,"SAUVGARDAGE DES DONNEES" , Toast.LENGTH_LONG).show();
                        }
        });





    }


}
