package com.example.uapv1900103.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public void onClick(View v) {

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CountryList cn = new CountryList();
        final ListView listview = (ListView) findViewById(R.id.lstView);
        String[] values =
                cn.getNameArray();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, values);

        listview.setAdapter(adapter);
        listview.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView parent, View v, int position, long id) {
                        Intent intent = new Intent(MainActivity.this, CountryActivity.class);
                        final String item = (String) parent.getItemAtPosition(position);
                        Bundle bn = new Bundle();
                        bn.putString("pays",item);
                        intent.putExtra("cp",bn);
                        startActivity(intent);

                    }
                });
    }

    private void setSupportActionBar(Toolbar toolbar) {
    }
}